package com.tsc.afedorovkaritsky.tm.controller;

import com.tsc.afedorovkaritsky.tm.api.controller.IProjectController;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectService;
import com.tsc.afedorovkaritsky.tm.enumerated.Sort;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.exception.entity.ProjectNotFoundException;
import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[Список проектов]");
        System.out.println("[Укажите вид сортировки]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + "." + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[Созать проект]");
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание проекта]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Введите ID проекта]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[Введите ид]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(id, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusProjectById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusProjectById(id, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusProjectByName(name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusProjectByIndex(index, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    public void showProjectById() {
        System.out.println("[Введите ID проекта]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    private void showProject(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        ;
        System.out.println("ID: " + project.getId());
        System.out.println("Название: " + project.getName());
        System.out.println("Описание: " + project.getDescription());
        System.out.println("Статус: " + project.getStatus());
    }

}
