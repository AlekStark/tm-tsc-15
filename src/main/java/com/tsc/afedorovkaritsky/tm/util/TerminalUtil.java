package com.tsc.afedorovkaritsky.tm.util;

import com.tsc.afedorovkaritsky.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
