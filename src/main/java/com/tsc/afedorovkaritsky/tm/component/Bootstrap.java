package com.tsc.afedorovkaritsky.tm.component;

import com.tsc.afedorovkaritsky.tm.api.controller.ICommandController;
import com.tsc.afedorovkaritsky.tm.api.controller.IProjectController;
import com.tsc.afedorovkaritsky.tm.api.controller.IProjectTaskController;
import com.tsc.afedorovkaritsky.tm.api.controller.ITaskController;
import com.tsc.afedorovkaritsky.tm.api.repository.ICommandRepository;
import com.tsc.afedorovkaritsky.tm.api.repository.IProjectRepository;
import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.api.service.ICommandService;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectService;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectTaskService;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.constant.ArgumentConst;
import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;
import com.tsc.afedorovkaritsky.tm.controller.CommandController;
import com.tsc.afedorovkaritsky.tm.controller.ProjectController;
import com.tsc.afedorovkaritsky.tm.controller.ProjectTaskController;
import com.tsc.afedorovkaritsky.tm.controller.TaskController;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.exception.system.UnknownArgumentException;
import com.tsc.afedorovkaritsky.tm.exception.system.UnknownCommandException;
import com.tsc.afedorovkaritsky.tm.repository.CommandRepository;
import com.tsc.afedorovkaritsky.tm.repository.ProjectRepository;
import com.tsc.afedorovkaritsky.tm.repository.TaskRepository;
import com.tsc.afedorovkaritsky.tm.service.CommandService;
import com.tsc.afedorovkaritsky.tm.service.ProjectService;
import com.tsc.afedorovkaritsky.tm.service.ProjectTaskService;
import com.tsc.afedorovkaritsky.tm.service.TaskService;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectService, taskService, projectTaskService, taskController);

    private final IProjectController projectController = new ProjectController(projectService);

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        initData();
        while (true) {
            try {
                System.out.println("Введите команду:");
                final String command = TerminalUtil.nextLine();
                parseCommand(command);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void initData() {
        projectService.create("Project A", "-").setStatus(Status.IN_PROGRESS);
        projectService.create("Project C", "-");
        projectService.create("Project D", "-").setStatus(Status.COMPLETED);
        projectService.create("Project B", "-");
        taskService.create("Task 1", "-");
        taskService.create("Task 4", "-").setStatus(Status.IN_PROGRESS);
        taskService.create("Task 3", "-");
        taskService.create("Task 2", "-").setStatus(Status.COMPLETED);
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.REMOVE_PROJECT_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.REMOVE_PROJECT_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.REMOVE_PROJECT_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.UPDATE_PROJECT_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.UPDATE_PROJECT_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeStatusProjectByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusProjectById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusProjectByIndex();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.REMOVE_TASK_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.REMOVE_TASK_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.REMOVE_TASK_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.UPDATE_TASK_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.UPDATE_TASK_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeStatusTaskByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusTaskById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusTaskByIndex();
                break;
            case TerminalConst.TASK_ADD_TO_PROJECT_BY_ID:
                projectTaskController.bindTaskToProjectById();
                break;
            case TerminalConst.TASK_REMOVE_TO_PROJECT_BY_ID:
                projectTaskController.unbindTaskFromProjectById();
                break;
            case TerminalConst.SHOW_TASK_BY_PROJECT_ID:
                projectTaskController.findAllTasksByProjectId();
                break;
            case TerminalConst.REMOVE_PROJECT_AND_TASK_BY_PROJECT_ID:
                projectTaskController.removeProjectById();
                break;
            case TerminalConst.REMOVE_PROJECT_AND_TASK_BY_PROJECT_INDEX:
                projectTaskController.removeProjectByIndex();
                break;
            case TerminalConst.REMOVE_PROJECT_AND_TASK_BY_PROJECT_NAME:
                projectTaskController.removeProjectByName();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                throw new UnknownCommandException(command);
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                throw new UnknownArgumentException(arg);
        }
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}
