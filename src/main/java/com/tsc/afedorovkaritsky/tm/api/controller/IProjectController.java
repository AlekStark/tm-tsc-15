package com.tsc.afedorovkaritsky.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void showProjectById();

    void showProjectByName();

    void showProjectByIndex();

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByName();

    void startProjectByIndex();

    void finishProjectById();

    void finishProjectByName();

    void finishProjectByIndex();

    void changeStatusProjectById();

    void changeStatusProjectByName();

    void changeStatusProjectByIndex();

}
