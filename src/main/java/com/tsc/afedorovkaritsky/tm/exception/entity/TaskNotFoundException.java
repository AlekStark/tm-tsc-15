package com.tsc.afedorovkaritsky.tm.exception.entity;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task was not found...");
    }

}
