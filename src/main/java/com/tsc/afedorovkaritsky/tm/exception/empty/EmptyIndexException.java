package com.tsc.afedorovkaritsky.tm.exception.empty;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error! Index is empty...");
    }

}
