package com.tsc.afedorovkaritsky.tm.exception.system;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String command) {
        super("Error! Command '" + command + "' was not found! Enter command ''help''...");
    }

}

