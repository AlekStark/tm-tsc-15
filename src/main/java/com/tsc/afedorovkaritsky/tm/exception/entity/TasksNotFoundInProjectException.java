package com.tsc.afedorovkaritsky.tm.exception.entity;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class TasksNotFoundInProjectException extends AbstractException {

    public TasksNotFoundInProjectException(final String projectId) {
        super("Error! No tasks in project " + projectId + "...");
    }

}
