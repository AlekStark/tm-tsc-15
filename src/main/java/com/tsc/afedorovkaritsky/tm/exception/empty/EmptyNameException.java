package com.tsc.afedorovkaritsky.tm.exception.empty;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}

