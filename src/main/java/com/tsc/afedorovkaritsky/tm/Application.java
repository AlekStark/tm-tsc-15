package com.tsc.afedorovkaritsky.tm;

import com.tsc.afedorovkaritsky.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
